from sklearn.metrics.pairwise import euclidean_distances
from sklearn.preprocessing import normalize
from sklearn.decomposition import NMF
from sklearn.manifold import MDS
from similarity.jarowinkler import JaroWinkler

from nimfa.utils import linalg
import numpy as np
import scipy

import pickle
import json
import re
import os
import string

t1 = 2008
tn = 2018


alphabet = list(string.ascii_lowercase)
charidx = dict([(a, e) for e, a in enumerate(alphabet)])
idxchar = dict([(e, a) for e, a in enumerate(alphabet)])
n_alphabet = len(alphabet) 

# Work dir structure
work_dir = 'D:\\dev\\phd\\papers\\phd-paper-01-followabilityScore\\src'
data_dir = 'D:\\dev\\phd\\dataset\\web_of_science\\LIS_20190416'
pickle_dir = work_dir + '\\pickled'
json_dir   = work_dir + '\\json'
joblib_dir = work_dir + '\\joblib'
foo = data_dir + '\\dataset_LIS.txt'

# Load previously stored data
ds = json.loads(open('{0}\\ds.json'.format(json_dir)).read())
auths = json.loads(open('{0}\\auths.json'.format(json_dir)).read())

C = pickle.load(open('{0}\\C.pickle'.format(pickle_dir), 'rb'))

colidx = pickle.load(open('{0}\\colidx.pickle'.format(pickle_dir), 'rb'))
idxcol = dict([(colidx[k], k) for k in colidx])

aliases = pickle.load(open('{0}\\aliases.pickle'.format(pickle_dir), 'rb'))


# Pickle some variables for future use
def picklelize(v, vname):
	with open('{0}/{1}.pickle'.format(pickle_dir, vname), 'wb') as handle:
		pickle.dump(v, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return

# Store dictionaries as JSON
def jsonize(v, vname):
    with open('{0}/{1}.json'.format(json_dir, vname), 'w') as json_file:
        #json.dump(v, json_file, indent=4)
        json.dump(v, json_file)
    return


# Alias array lookup version
def cosine_similarity(s1_, s2_):
    s1 = ''.join(re.split(r'\W+', s1_)).replace('_', '')
    s2 = ''.join(re.split(r'\W+', s2_)).replace('_', '')
    max_name_length = max([len(k) for k in [s1_, s2_]])
    char_vec_length = n_alphabet * (max_name_length + 1)
    arr = [s1, s2] if len(s1) >= len(s2) else [s2, s1]
    
    Z = np.matrix(np.zeros((2, char_vec_length)))
    for i, name in enumerate(arr):
        char_list = list(name)
        for e, char in enumerate(char_list):
            idx = charidx[char]
            j = e * n_alphabet + idx
            Z[i, j] = 1.0
    
    ZdotZ = Z * Z.T
    diag = np.sqrt(ZdotZ).diagonal().reshape((2,1))
    res = ZdotZ[0,1] / (diag[0, 0] * diag[1, 0])
    return res


def isinitial(s):
    res = True
    s = s.split('_')[1].replace('.', '')
    if len(s) > 2:
        res = False
    return res


def toinitial(s):
    auparts = s.split('_')
    surname = auparts[0]
    tmp = []
    for k1 in auparts[1:]:
        if '.' in k1:
            tmp.append('.'.join([k2[0] for k2 in k1.split('.') if len(k2) > 0]))
        else:
            tmp.append(k1[0])
    initials = '.'.join([k for k in tmp])
    return surname + '_' + initials


def name_similarity(u, v):
    (u, v) = (u, v) if len(u) >= len(v) else (v, u)
    sim1 = cosine_similarity(u, v)
    #print(u, v, sim1)
    res = sim1
    ucheck = isinitial(u)
    if ucheck==False and u != v:
        u = toinitial(u)
        sim2 = cosine_similarity(u, v)
        #print(u, v, sim2)
        res = (sim1 + sim2)/2.0
    return res



# Construct synonym lookup
synonyms = {}
for k in aliases:
    alias = aliases[k]
    if alias in synonyms:
        synonyms[alias] = synonyms[alias] + [k]
    else:
        synonyms[alias] = [k]
picklelize(synonyms, 'synonym_candidates')


# Jaro-Winkler similarity
def jwsim(target):
    jarowinkler = JaroWinkler()
    test_list = synonyms[target]
    res = []
    for k in test_list:
        jws = jarowinkler.similarity(target, k)
        if jws > 0.95:
            res.append((k, jws))
    return res


# Cosine similarity
def nsim(target):
    test_list = synonyms[target]
    res = []
    for k in test_list:
        ns = name_similarity(target, k)
        print(target, k, ns)
        if ns > 0.80:
            res.append((k, ns))
    return res

# Revise to closest matches
for k in synonyms:
    #valid = [k[0] for k in jwsim(k)]
    valid = [k[0] for k in nsim(k)]
    synonyms[k] = valid


# Revise rowidx, idxrow dictionary to reflect occurence of synonyms
pids = {}
for k in synonyms:
    for alias in synonyms[k]:
        lst = [ut for ut in auths[alias]]
        if k in pids:
            pids[k] = pids[k] + lst
        else:
            pids[k] = lst


ridx = dict([(k, e) for e, k in enumerate(pids)])
idxr = dict(zip(ridx.values(), ridx.keys()))

M2 = scipy.sparse.lil_matrix((len(ridx),len(colidx)))
for i, a in enumerate(ridx):
    for k in pids[a]:
        M2[i, colidx[k]] = 1
M2 = M2.tocsr()

picklelize(pids, 'pids')
picklelize(ridx, 'ridx')
picklelize(synonyms, 'synonyms')
picklelize(M2, 'M2')


"""
Step 1: ingest-wos_first-pass.py
Step 2: synonym.py
Step 3: ingest-wos_second-pass.py
"""


