from sklearn.metrics.pairwise import euclidean_distances
from sklearn.preprocessing import normalize
from sklearn.decomposition import NMF
from sklearn.manifold import MDS
from similarity.jarowinkler import JaroWinkler

from nimfa.utils import linalg
import numpy as np
import scipy
import pandas as pd

import pickle
import json
import re
import os
import string

t1 = 2008
tn = 2018

# Work dir structure
work_dir = 'D:\\dev\\phd\\papers\\phd-paper-01-followabilityScore\\src'
data_dir = 'D:\\dev\\phd\\dataset\\web_of_science\\LIS_20190416'
pickle_dir = work_dir + '\\pickled'
json_dir   = work_dir + '\\json'
joblib_dir = work_dir + '\\joblib'
foo = data_dir + '\\dataset_LIS.txt'

# Load previously stored data
ds = json.loads(open('{0}\\ds.json'.format(json_dir)).read())
auths = json.loads(open('{0}\\auths.json'.format(json_dir)).read())

# Load sparse matrices
L = pickle.load(open('{0}\\C.pickle'.format(pickle_dir), 'rb'))
M = pickle.load(open('{0}\\M2.pickle'.format(pickle_dir), 'rb'))

# Normalized author-paper matrix
W = normalize(M, axis=0, norm='l1')

# Author-reference matrix: r-matrix
r = W * L

# Matrix dimensions
m, n = W.shape

# Load pickled look-up dictionaries
idpub = pickle.load(open('{0}\\idpub.pickle'.format(pickle_dir), 'rb'))
pubid = dict([(idpub[k], k) for k in idpub])

cidx = pickle.load(open('{0}\\colidx.pickle'.format(pickle_dir), 'rb'))
idxc = dict([(cidx[k], k) for k in cidx])

ridx = pickle.load(open('{0}\\ridx.pickle'.format(pickle_dir), 'rb'))
idxr = dict([(ridx[k], k) for k in ridx])

synonyms = pickle.load(open('{0}\\synonyms.pickle'.format(pickle_dir), 'rb'))

# Pickle some variables for future use
def picklelize(v, vname):
	with open('{0}/{1}.pickle'.format(pickle_dir, vname), 'wb') as handle:
		pickle.dump(v, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return

# Store dictionaries as JSON
def jsonize(v, vname):
    with open('{0}/{1}.json'.format(json_dir, vname), 'w') as json_file:
        #json.dump(v, json_file, indent=4)
        json.dump(v, json_file)
    return


# Bibliographic coupling / co-reference matrix:
R = L * L.T
D = scipy.sparse.lil_matrix(R.shape)
Rdiag = R.diagonal()

nzRdiag = Rdiag.nonzero()[0]
for k in nzRdiag:
    D[k, k] = Rdiag[k]
D = D.tocsr()

R.setdiag(0)

WDWt = W * D * W.T
WRWt = W * R * W.T
omega = WDWt + WRWt

ones = np.matrix(np.ones((M.shape[0], 1)))

omega1 = WDWt * ones
d_omega1 = dict([(k, omega1[ridx[k], 0]) for k in ridx])

omega2 = WRWt * ones
d_omega2 = dict([(k, omega2[ridx[k], 0]) for k in ridx])

def author_follow_score(K):
    tol = 0.5e-6
    x = np.matrix(np.ones((m, 1)))
    max_diff = 1.0
    iters = 0
    while max_diff > tol:
        xk =  K * x
        norm_xk = np.matrix(normalize(xk, axis=0, norm='l1'))
        abs_diff = np.abs(x - norm_xk)
        max_diff = np.max(abs_diff)
        x = norm_xk
        iters += 1
        print("[+] Iteration: {0:d} | max(|a - a'|) = {1:.3e}".format(iters, max_diff))
    print('CAPS author, document vector length = ({0[0]:d},{0[1]:02d}) | Iterations: {1:d} @ tol = {2:.1e}'.format((m, n), iters, tol))
    score_a = dict([(idxr[k], x[k,0]) for k in range(m)])
    return score_a


# Run algorithm
afs = author_follow_score(omega)

# No. of publications
rsM = M.sum(axis=1)
npubs = dict([(k, rsM[ridx[k], 0]) for k in ridx])

# No. of citations
MLt = M * L.T
rsMLt = MLt.sum(axis=1)
ncites = dict([(k, rsMLt[ridx[k], 0]) for k in ridx])

# No. of references
ML = M * L
rsML = ML.sum(axis=1)
nrefs = dict([(k, rsML[ridx[k], 0]) for k in ridx])

# No. of coauthors
A = M * M.T
A[A > 0] = 1.0
A.setdiag(0)
rsA = A.sum(axis=1)
ncoauths = dict([(k, rsA[ridx[k], 0]) for k in ridx])

# Create dataframe for top 20 authors
df_a = pd.DataFrame(sorted(afs.items(), key=lambda x:x[1], reverse=True)[0:20], columns=['Name', 'FollowScore'])
df_a['nPubs'] = df_a.Name.apply(lambda x: npubs[x])
df_a['nCites'] = df_a.Name.apply(lambda x: ncites[x])
df_a['nRefs'] = df_a.Name.apply(lambda x: nrefs[x])
df_a['nCoauths'] = df_a.Name.apply(lambda x: ncoauths[x])

# Get frequency of collaborations between top 20 authors
lst_a = [k[0] for k in sorted(afs.items(), key=lambda x:x[1], reverse=True)[0:20]]
aidx = dict([(k, e + 1) for e, k in enumerate(lst_a)])
MMt = M * M.T
MMt.setdiag(0)

def get_coauths(i_):
    res = [k[0] for k in sorted([(aidx[idxr[j]], MMt[ridx[i_], j]) for j in MMt[ridx[i_],:].nonzero()[1] if idxr[j] in lst_a], key=lambda x:x[1], reverse=True)]
    return res
print([(i, get_coauths(i)) for i in lst_a])


def get_corefauths(i_):
    res = [aidx[k[0]] for k in sorted([(idxr[j], omega[j, ridx[i_]]) for j in omega[:, ridx[i_]].nonzero()[0] if idxr[j] in aidx], key=lambda x:x[1], reverse=True)][0:5]
    return res

df_a['Corefauths'] = df_a.Name.apply(lambda x: get_corefauths(x))

def get_simauths(i_):
    #tmp = sorted([(idxr[j], cos_theta_a[ridx[i_], j]) for j in cos_theta_a[ridx[i_],:].nonzero()[1] if idxr[j] not in aidx], key=lambda x:x[1], reverse=True)[0:3]
    tmp = sorted([(idxr[j], cos_theta_a[ridx[i_], j]) for j in cos_theta_a[ridx[i_],:].nonzero()[1]], key=lambda x:x[1], reverse=True)[0:3]
    res = ';'.join(['{0} {1:.3f}, {2}'.format(k[0], k[1], A[ridx[i_], ridx[k[0]]]) for k in tmp])
    return res

cos_theta_a = pickle.load(open('{0}\\cos_theta_a.pickle'.format(pickle_dir), 'rb'))

df_a['SimAuths'] = df_a.Name.apply(lambda x: get_simauths(x))
df_a.to_csv('top20_authors.csv')

# Picklelize results
picklelize(afs, 'author_followscore')


"""
ingest-wos_first-pass.py
synonymy.py
ingest-wos_second-pass.py
authref_similarity.py
author_followscore.py
"""
