ACN = M * L * M.T   # author citation network
A = W * L * W.T  # citations are conserved
AAT = WACN * WACN.T
ATA = WACN.T * WACN

def hits_score():
    tol = 0.5e-6
    x = np.matrix(np.ones((m, 1)))
    y = np.matrix(np.ones((m, 1)))
    max_diff = 1.0
    iters = 0
    while max_diff > tol:
        xk = AAT * x # authority score
        yk = ATA * y # hub score
        norm_xk = np.matrix(normalize(xk, axis=0, norm='l1'))
        norm_yk = np.matrix(normalize(yk, axis=0, norm='l1'))
        abs_diffx = np.abs(x - norm_xk)
        abs_diffy = np.abs(y - norm_yk)
        max_diffx = np.max(abs_diffx)
        max_diffy = np.max(abs_diffy)
        max_diff = np.max([max_diffx, max_diffy])
        x = norm_xk
        y = norm_yk
        iters += 1
        print("[+] Iteration: {0:d} | max(|a - a'|) = {1:.3e}".format(iters, max_diff))
    print('CAPS author, document vector length = ({0[0]:d},{0[1]:02d}) | Iterations: {1:d} @ tol = {2:.1e}'.format((m, n), iters, tol))
    score_a = dict([(idxr[k], x[k,0]) for k in range(m)])
    score_h = dict([(idxr[k], y[k,0]) for k in range(m)])
    return score_a, score_h

authscore, hubscore = hits_score()
