from sklearn.metrics.pairwise import euclidean_distances
from sklearn.preprocessing import normalize
from sklearn.decomposition import NMF
from sklearn.manifold import MDS
from similarity.jarowinkler import JaroWinkler

from nimfa.utils import linalg
import numpy as np
import scipy

import pickle
import json
import re
import os
import string

t1 = 2008
tn = 2018

# Work dir structure
work_dir = 'D:\\dev\\phd\\papers\\phd-paper-01-followabilityScore\\src'
data_dir = 'D:\\dev\\phd\\dataset\\web_of_science\\LIS_20190416'
pickle_dir = work_dir + '\\pickled'
json_dir   = work_dir + '\\json'
joblib_dir = work_dir + '\\joblib'
foo = data_dir + '\\dataset_LIS.txt'

# Load previously stored data
ds = json.loads(open('{0}\\ds.json'.format(json_dir)).read())
auths = json.loads(open('{0}\\auths.json'.format(json_dir)).read())

# Load sparse matrices
C = pickle.load(open('{0}\\C.pickle'.format(pickle_dir), 'rb'))
M = pickle.load(open('{0}\\M2.pickle'.format(pickle_dir), 'rb'))

# Normalized author-paper matrix
W = normalize(M, axis=0, norm='l1')

# L-matrix
L = W * C

# Matrix dimensions
m, n = W.shape

# Load pickled look-up dictionaries
idpub = pickle.load(open('{0}\\idpub.pickle'.format(pickle_dir), 'rb'))
pubid = dict([(idpub[k], k) for k in idpub])

cidx = pickle.load(open('{0}\\colidx.pickle'.format(pickle_dir), 'rb'))
idxc = dict([(cidx[k], k) for k in cidx])

ridx = pickle.load(open('{0}\\ridx.pickle'.format(pickle_dir), 'rb'))
idxr = dict([(ridx[k], k) for k in ridx])

synonyms = pickle.load(open('{0}\\synonyms.pickle'.format(pickle_dir), 'rb'))

followscore_a = pickle.load(open('{0}\\followscore_a.pickle'.format(pickle_dir), 'rb'))
followscore_p = pickle.load(open('{0}\\followscore_p.pickle'.format(pickle_dir), 'rb'))


# Author recommendation
LLT = L * L.T
diagLLT = np.sqrt(LLT).diagonal().reshape((m, 1))

cos_theta_a = scipy.sparse.lil_matrix((m, m))
for i in range(m):
	print('{0:05d} {1}'.format(i, idxr[i]))
	for j in LLT[i, :].nonzero()[1]:
		cos_theta_a[i, j] = LLT[i, j]/(diagLLT[i, 0] * diagLLT[j, 0])

# Paper recommendation
LTL = L.T * L
diagLTL = np.sqrt(LTL).diagonal().reshape((n, 1))

cos_theta_p = scipy.sparse.lil_matrix((n, n))
for i in range(n):
	print('{0:06d} {1}'.format(i, idxc[i]))
	for j in LTL[i, :].nonzero()[1]:
		cos_theta_p[i, j] = LTL[i, j]/(diagLTL[i, 0] * diagLTL[j, 0])


# Pickle some variables for future use
def picklelize(v, vname):
	with open('{0}/{1}.pickle'.format(pickle_dir, vname), 'wb') as handle:
		pickle.dump(v, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return

# Store dictionaries as JSON
def jsonize(v, vname):
    with open('{0}/{1}.json'.format(json_dir, vname), 'w') as json_file:
        #json.dump(v, json_file, indent=4)
        json.dump(v, json_file)
    return

picklelize(cos_theta_a, 'cos_theta_a')
picklelize(cos_theta_p, 'cos_theta_p')


"""
ingest-wos_first-pass.py
synonymy.py
ingest-wos_second-pass.py
authref_similarity.py
"""