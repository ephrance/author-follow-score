# Scoring the resourcefulness of researchers using co-reference patterns

Python 3 source code used for this paper are stored on this repository. It is split into six main scripts:

- `ingest-wos_first-pass.py`
- `synonymy.py`
- `ingest-wos_second-pass.py`
- `authref_similarity.py`
- `author_followscore.py`
- `hits.py`

which correspond to major workflows needed to compute the author follow-scores and author co-reference similarity.

Do take note that the Web of Science data used are not included here. You may need to download your own datasets and modify the code accordingly to load the data.

To run the entire workflow, execute the `workflow.py` script either from command line or from Python/IPython console:
```
exec(open('workflow.py').read())
```

Windows was used to develop the source code using the Anaconda 3 distribution. Some conversion to Linux filepath conventions are needed to run on Linux - look for directory declarations at the start of each script.

