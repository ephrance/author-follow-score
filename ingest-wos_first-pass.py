from collections import defaultdict
from itertools import combinations
from time import clock
import csv
import re
import os

import numpy as np
import networkx as nx
import scipy

from sklearn.metrics.pairwise import euclidean_distances
from sklearn.preprocessing import normalize
from sklearn.decomposition import NMF
from sklearn.manifold import MDS

from nimfa.utils import linalg

from joblib import dump, load
import pickle
import json

t1 = 2008
tn = 2018

work_dir = 'D:\\dev\\phd\\papers\\phd-paper-01-followabilityScore\\src'
data_dir = 'D:\\dev\\phd\\dataset\\web_of_science\\LIS_20190416'
pickle_dir = work_dir + '/pickled'
json_dir   = work_dir + '/json'
joblib_dir = work_dir + '/joblib'
foo = data_dir + '/dataset_LIS.txt'
#foo = 'D:\\dev\\phd\\dataset\\web_of_science\\LIS_20190416\\dataset_LIS.txt'

regx = re.compile('^([^ ]{2,2}) (.*)')
regx_cref = re.compile("([^,]+), ([0-9]{4,}), ([^,]+), v([0-9]+), p([0-9]+)(.*)")


def despace(s):
   return re.sub("\s+", '.', s);


def get_reference(s):
   val = regx_cref.search(s.lower())
   if val is not None:
      au = val.group(1)
      py = val.group(2)
      j9 = val.group(3)
      vl = 'v' + val.group(4)
      bp = 'p' + val.group(5)

      au_parts = au.split(' ')
      surname = au_parts[0]
      initials = ''.join(au_parts[1:])
      initials = re.sub('\.', '', initials)
      au1 = surname + '_' + initials
      j9 = despace(j9)
      res = '|'.join([au1, py, j9, vl, bp])
   else:
      res = ''
   return res;


def get_pub_id(k, d):
    res = ''
    if set(['AU', 'PY', 'J9', 'VL', 'BP']).issubset(d[k].keys()) and d[k]['AU']:
        au = d[k]['AU']
        if len(au) > 1:
            au = d[k]['AU'].split('; ')[0]
        py = ''
        if d[k]['PY'] != []:
            py = str(d[k]['PY'])
        elif d[k]['EY'] != []:
            py = str(d[k]['EY'])
        j9 = despace(d[k]['J9'])
        vl = 'v' + d[k]['VL']
        bp = 'p' + d[k]['BP']
        au1 = au
        if ',' in au:
            (surname, initials) = au1.split(',')
            initials = re.sub(r'^ ', '', initials)
            initials = despace(initials)
            au1 = surname + '_' + initials
            res = '|'.join([au1, py, j9, vl, bp]).lower()
        if au1.lower() == '[anonymous]':
            res = ''
    return res


def gen_auth_keyword(s, mode):
    res = ''
    s = re.sub(r'(, )\1+', r'\1', s.lower())
    if mode == 0:
        if ',' in s:
            (surname, initials) = s.split(',')
            initials = re.sub(r'^ ', '', initials)
            initials = '.'.join(list(initials))
            surname = re.sub('\s+', '.', surname)
            res = '{0}_{1}'.format(surname, initials)
        else:
            au_parts = s.split(' ')
            surname = au_parts[0]
            initials = '.'.join(au_parts[1:])
            res = '{0}_{1}'.format(surname, initials)
    elif mode == 1:
        if ',' in s:
            s = despace(re.sub(',', '_', s))
            s = re.sub('-', '.', s)
            s = re.sub('\.+', '.', s)
            s = re.sub('\.$', '', s)
            res = re.sub('_.', '_', s)
        else:
            au_parts = s.split(' ')
            surname = au_parts[0]
            initials = '.'.join(au_parts[1:])
            res = '{0}_{1}'.format(surname, initials)
    return res


def get_auth_keywords(k, d):
    res = []
    au_data = d[k]['AU'].split('; ')
    af_data = d[k]['AF'].split('; ')
    for au, af in zip(au_data, af_data):
        if af == au:
            kw = gen_auth_keyword(af, 0)
        else:
            kw = gen_auth_keyword(af, 1)
        if kw != '':
            res.append(kw)
    return res


def gen_citation_net(d, lst):
	G = nx.DiGraph()
	for k in d.keys():
		id = get_pub_id(k, d)
		if d[k]['CR'] and id != '':
			G.add_node(k)
			crefs = d[k]['CR'].split('; ')
			for cr in crefs:
				cref = get_reference(cr)
				if cref in lst:
					kref = lst[cref]
					G.add_edge(k, kref)
	print('Document citation network G: |V| = {0:d}, |E| = {1:d}'.format(G.number_of_nodes(), G.number_of_edges()))
	return G


def load_data(foo):
	data = defaultdict(list)
	rec = defaultdict(lambda: defaultdict(list)) 
	oldKey = ''
	UT = ''
	collect = ''
	for e, line in enumerate( open(foo, encoding='utf-8') ):
		matchStr = regx.search(line)
		if matchStr:
			key = matchStr.group(1)
			val = matchStr.group(2)
			if key == 'UT':
				UT = val
				print(UT)
			else:
				data[key] = val
			oldKey = key
		else:
			if oldKey not in ('', 'UT'):
				if oldKey not in ('AU', 'AF', 'C1', 'CR'):
					data[oldKey] = data.pop(oldKey) + ' ' + line.strip()
				elif oldKey == 'C1':
					data[oldKey] = data.pop(oldKey) + ' ++ ' + line.strip()
				else:
					data[oldKey] = data.pop(oldKey) + '; ' + line.strip()
			if line == 'ER\n':
				for k, v in data.items():
					rec[UT][k] = v
				data.clear()
				oldKey = ''
				UT = ''
				collect = ''
	print('Loaded {0:d} records'.format(len(rec)))
	return rec


def pubdicts(d):
	dictionary = {}
	for k in d:
		pid = get_pub_id(k, d)
		if pid:
			dictionary[k] = pid
	return dictionary


# Load ISI data
recs = load_data(foo)
k_list = []
for k in recs:
    if recs[k]['AF'] != []: 
        py = ''
        if 'PY' in recs[k] and recs[k]['PY'] != []:
            py = int(recs[k]['PY'])
        elif 'EY' in recs[k] and recs[k]['EY'] != []:
            py = int(recs[k]['EY'])
        if py >= t1 and py <= tn:
            k_list.append(k)

ds = dict([(k, recs[k]) for k in k_list])

idpub = pubdicts(ds)
pubid = dict([(idpub[k], k) for k in idpub])

# Paper citation matrix, C
G = gen_citation_net(ds, pubid)
C = nx.adjacency_matrix(G)
colidx = dict([(k, e) for e, k in enumerate(G.nodes())])
idxcol = dict(zip(colidx.values(), colidx.keys()))
print('C matrix dimensions: ({0[0]:d},{0[1]:02d})'.format(C.shape))

# Author-paper lookup
auths = defaultdict( lambda: defaultdict() )
for k in G.nodes():
    au = get_auth_keywords(k, ds)
    for a in au:
        auths[a][k] = 1


rowidx = defaultdict(int)
M = scipy.sparse.lil_matrix((len(auths),len(colidx)))
idx = 0
for a in sorted(auths):
    rowidx[a] = idx
    for k in auths[a]:
        M[idx, colidx[k]] = 1
    idx += 1
idxrow = dict(zip(rowidx.values(), rowidx.keys()))

print('M matrix dimensions: ({0[0]:d},{0[1]:02d})'.format(M.shape))

# Normalized author-paper matrix
W = normalize(M, axis=0, norm='l1').tolil()

# Authorship matrix
A = M * M.T
A.setdiag(0)


# Pickle some variables for future use
def picklelize(v, vname):
	with open('{0}/{1}.pickle'.format(pickle_dir, vname), 'wb') as handle:
		pickle.dump(v, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return

picklelize(idpub, 'idpub')
picklelize(G, 'G')
picklelize(C, 'C')
picklelize(M, 'M')
picklelize(colidx, 'colidx')
picklelize(rowidx, 'rowidx')

# Store dictionaries as JSON
def jsonize(v, vname):
    with open('{0}/{1}.json'.format(json_dir, vname), 'w') as json_file:
        #json.dump(v, json_file, indent=4)
        json.dump(v, json_file)
    return

jsonize(ds, 'ds')
jsonize(auths, 'auths')

