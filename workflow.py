# Step 1: Load Web of Science dataset (not included) as first pass without author synonym correction
exec(open('ingest-wos_first-pass.py').read())


# Step 2: Find author synonyms
exec(open('synonymy.py').read())


# Step 3: Reload Web of Science dataset taking into account detected author synonyms
exec(open('ingest-wos_second-pass.py').read())


# Step 4: Compute author follow-score + author co-reference similarity
## Output: top20_authors.csv
exec(open('authref_similarity.py').read())
exec(open('author_followscore.py').read())

# Step 5: Compute HITS hub score on author citation network for validation
exec(open('hits.py').read())

