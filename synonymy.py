import pickle
import numpy as np
import scipy
import json
import os
import re

# Work dir structure
work_dir = 'D:\\dev\\phd\\papers\\phd-paper-01-followabilityScore\\src'
data_dir = 'D:\\dev\\phd\\dataset\\web_of_science\\LIS_20190416'
pickle_dir = work_dir + '\\pickled'
json_dir   = work_dir + '\\json'
joblib_dir = work_dir + '\\joblib'
foo = data_dir + '\\dataset_LIS.txt'

# Load previously stored data
ds = json.loads(open('{0}\\ds.json'.format(json_dir)).read())
auths = json.loads(open('{0}\\auths.json'.format(json_dir)).read())

M = pickle.load(open('{0}\\M.pickle'.format(pickle_dir), 'rb'))

rowidx = pickle.load(open('{0}\\rowidx.pickle'.format(pickle_dir), 'rb'))
idxrow = dict([(rowidx[k], k) for k in rowidx])

name_parts = []
np_dict = {}
for a in auths:
    a_dots = re.sub('_', '_.', a)
    a_parts = [re.sub(r'\W+', '', k) for k in a_dots.split('.')]
    np_dict[a] = [k for k in a_parts if len(k) > 0]
    for ap in a_parts:
        if len(ap) > 0:
            name_parts.append(ap)

#name_parts = list(np.unique(name_parts))[1:-1]
name_parts = list(np.unique(name_parts))

N = len(name_parts)
idxnp = dict([(e,n) for e, n in enumerate(name_parts)])
npidx = dict([(n,e) for e, n in enumerate(name_parts)])

# name part matrix
m = M.shape[0]
n = M.shape[1]

P = scipy.sparse.lil_matrix((m, N))
for e, a in enumerate(auths):
    print('{0:05d} {1}'.format(e, a))
    i = rowidx[a]
    a_parts = np_dict[a]
    for ap in a_parts:
        j = npidx[ap]
        if '_' in ap:
            P[i, j] = 1 
        else:
            P[i, j] = 0.1

S = P * P.T
d = np.sqrt(S).diagonal().reshape((m, 1))

cos_theta = scipy.sparse.lil_matrix((m, m))
for i in range(m):
	print('{0:05d} {1}'.format(i, idxrow[i]))
	for j in S[i, :].nonzero()[1]:
		cos_theta[i, j] = S[i, j]/(d[i,0] * d[j,0])


def diffstillsame(target, threshold):
    target_idx = rowidx[target]
    res = [(idxrow[j], cos_theta[target_idx, j]) for j in cos_theta[target_idx,:].nonzero()[1] if cos_theta[target_idx, j] >= threshold]
    return res


# Algorithm:
# 1. Generate candidate list with > 0.9 name part similarity
candidates = {}
for k in sorted(auths):
    print(k)
    tmp = diffstillsame(k, 0.9)
    candidates[k] = [(l,v) if l!=k else (l,-1.0) for l,v in tmp]

# 2. Loop over sort descending by name length
# 3. Check if not skip name, else skip name
# 4. Check if candidate in name and candidate not in skip name, else skip name

def abbreviate(s):
    res = s
    au_parts = [k for k in s.split('_') if len(k) > 0]
    if len(au_parts) > 1:
        surname = au_parts[0]
        initials = '_'.join(au_parts[1:])
        filter_W = [re.sub(r'\W+', '', k) for k in initials.split('.')]
        initials = '.'.join([k for k in filter_W if len(k) > 0])
        initials = ''.join([k[0] for k in initials.split('.')])
        res = surname + '_' + initials
    return res

lst = sorted([(k, len(k)) for k in candidates], key=lambda x:x[1], reverse=True)
aliases = dict([(k, k) for k in candidates])
skip = {}
for i, v in lst:
    n_candidates = len(candidates[i])
    i_abbrev = abbreviate(i)
    for j, sim_ij in sorted(candidates[i], key=lambda x:x[1], reverse=True):
        j_abbrev = abbreviate(j)
        if j not in skip:
            if j_abbrev == i_abbrev:
                aliases[j] = i
                skip[j] = 1
            elif j_abbrev in i_abbrev:
                aliases[j] = i
                skip[j] = 1
            elif j in i_abbrev:
                aliases[j] = i
                skip[j] = 1
            elif j in i:
                aliases[j] = i
                skip[j] = 1
    skip[i] = 1



# Pickle some variables for future use
def picklelize(v, vname):
	with open('{0}/{1}.pickle'.format(pickle_dir, vname), 'wb') as handle:
		pickle.dump(v, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return


picklelize(npidx, 'npidx')
picklelize(P, 'P')
picklelize(cos_theta, 'cos_theta')
picklelize(np_dict, 'np_dict')
picklelize(candidates, 'synonym_candidates')
picklelize(aliases, 'aliases')
